using FizzBuzz.Business;
using StructureMap;
namespace FizzBuzz.Web
{
    public static class IoC
    {
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize(x =>
            {
                x.For<IFizzBuzzManager>().Use<FizzBuzzManager>();
                x.For<IDivisionPolicy>().Use<FizzLogic>().SetProperty(a => a.Order = 2);
                x.For<IDivisionPolicy>().Use<BuzzLogic>().SetProperty(a => a.Order = 3);
                x.For<IDivisionPolicy>().Use<FizzBuzzLogic>().SetProperty(a => a.Order = 1);
                x.For<IDayProvider>().Use<DayProvider>();
            });
            return ObjectFactory.Container;
        }
    }
}