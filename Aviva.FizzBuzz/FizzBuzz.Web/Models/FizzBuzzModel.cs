﻿namespace FizzBuzz.Web.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    using Resources;

    public class FizzBuzzModel
    {
        [Required(ErrorMessageResourceType = typeof(UIResource), ErrorMessageResourceName = "InputErrorMessage")]
        [Range(1, 1000, ErrorMessageResourceType = typeof(UIResource), ErrorMessageResourceName = "RangeErrorMessage")]
        public int InputValue { get; set; }

        public IEnumerable<string> FizzBuzzValuesList { get; set; }

        public bool ShowNextLink { get; set; }

        public bool ShowPrevLink { get; set; }

        public int PageNumber { get; set; }
    }
}