﻿namespace FizzBuzz.Business
{
    using System.Collections.Generic;

    public interface IFizzBuzzManager
    {
        IEnumerable<string> GetFizzBuzzList(int inputValue);
    }
}
