﻿namespace FizzBuzz.Business
{
    using System;

    public class DayProvider : IDayProvider
    {
        public bool IsTodayWednesday()
        {
            return DateTime.Today.DayOfWeek == DayOfWeek.Wednesday;
        }
    }
}
